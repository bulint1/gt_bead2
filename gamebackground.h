#ifndef GAMEBACKGROUND_H
#define GAMEBACKGROUND_H
#include "element.h"
#include <QKeyEvent>
#include <QShortcut>
#include <QTimer>
#include <QVector>
#include <QWidget>

class gamebackgound : public QWidget {
  Q_OBJECT
public:
  gamebackgound();
  void start();
  void pause();
  bool getGameRunning() const;
  Element *getGameTable(int x, int y);
  void newGame(std::string map);
  void checkKey(Qt::Key key);
  int getScore();
  int getBoardSize() const;

private slots:
  void stepGame();

private:
  QVector<QVector<Element *>> gameTable;
  QVector<Guard *> guards; // végig iterálunk aztán firssítjük a gametablet
  QVector<Basket *> baskets;
  QTimer *frame;
  bool gameRunning;
  int boardSize;
  Laci *laci;
  void replace(Guard *guard);
  void checkGame();
  void readData(std::string file);
  void updatePosition();
  void replace(Laci *laci);
signals:
  void updateScreen();
  void gameWon();
  void gameLost();
};

#endif // GAMEBACKGROUND_H
