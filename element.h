#ifndef ELEMENT_H
#define ELEMENT_H
#include "direction.h"
#include <QPixmap>
#include <QVector>

class Element {
public:
  pos *getPosition() const;
  void setPosition(pos *value);

  bool getIsObsticle() const;
  void setIsObsticle(bool value);

  bool getIsLaci() const;
  void setIsLaci(bool value);

  QPixmap *getIm() const;

  // pos *getLastPosition() const;
  // void setLastPosition(pos *value);

protected:
  pos *position;
  bool isObsticle;
  bool isLaci;
  dir direction;
  // pos *lastPosition;
  QPixmap *im;
};
class Guard : public Element { // moved by gamebackground
public:
  Guard(int x, int y, dir dire);
  void canStep(const QVector<QVector<Element *>> &gameTable, int boardSize);
  bool checkAround(const QVector<QVector<Element *>> &gameTable,
                   int boardSize); // look for laci and turn around if needed

  pos *getLastPosition() const;
  void setLastPosition(pos *value);

  int getCounter() const;
  void setCounter(int value);

private:
  pos *lastPosition;
  // dir *direction;
  // int range;
  int counter;
  void reverseDirection();
  void step();
};
class Obstacle : public Element {
public:
  Obstacle(int x, int y);
};
class Laci : public Element {
public:
  Laci(int x, int y);

  bool getAlive() const;
  void die();
  void step(dir direction, int boardSize,
            const QVector<QVector<Element *>> &gameTable);

  pos *getLastPosition() const;
  void setLastPosition(pos *value);

  int getScore() const;
  void setScore(int value);
  void incScore();

private:
  bool alive;
  pos *lastPosition;
  int score;
};
class Empty : public Element {
public:
  Empty(int x, int y);
};

class Basket : public Element {
public:
  Basket(int x, int y);
};
#endif // ELEMENT_H
