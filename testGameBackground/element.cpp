#include "element.h"

pos *Element::getPosition() const { return position; }

void Element::setPosition(pos *value) { position = value; }

bool Element::getIsObsticle() const { return isObsticle; }

void Element::setIsObsticle(bool value) { isObsticle = value; }

bool Element::getIsLaci() const { return isLaci; }

void Element::setIsLaci(bool value) { isLaci = value; }

//QPixmap *Element::getIm() const { return im; }

Guard::Guard(int x, int y, dir dire) {
  position = new pos(x, y);
  lastPosition = nullptr;
  isObsticle = false;
  isLaci = false;
  direction = dire;
//  im = new QPixmap(":/img/img/guard.png");
  counter = 0;
}

bool Guard::checkAround(const QVector<QVector<Element *>> &gameTable,
                        int boardSize) {
  boardSize -= 1;
  int x = position->x_pos;
  int y = position->y_pos;
  bool res = false;
  if (!res && x < boardSize) {
    res = res || gameTable[x + 1][y]->getIsLaci();
    if (y < boardSize)
      res = res || gameTable[x + 1][y + 1]->getIsLaci();
    if (y > 0)
      res = res || gameTable[x + 1][y - 1]->getIsLaci();
  }
  if (!res && x > 0) {
    res = res || gameTable[x - 1][y]->getIsLaci();
    if (y < boardSize)
      res = res || gameTable[x - 1][y + 1]->getIsLaci();
    if (y > 0)
      res = res || gameTable[x - 1][y - 1]->getIsLaci();
  }
  if (!res && y < boardSize) {
    res = res || gameTable[x][y + 1]->getIsLaci();
    if (x < boardSize)
      res = res || gameTable[x + 1][y + 1]->getIsLaci();
    if (x > 0)
      res = res || gameTable[x - 1][y + 1]->getIsLaci();
  }
  if (!res && y > 0) {
    res = res || gameTable[x][y - 1]->getIsLaci();
    if (x < boardSize)
      res = res || gameTable[x + 1][y - 1]->getIsLaci();
    if (x > 0)
      res = res || gameTable[x - 1][y - 1]->getIsLaci();
  }
  return res;
}

pos *Guard::getLastPosition() const { return lastPosition; }

void Guard::setLastPosition(pos *value) { lastPosition = value; }

int Guard::getCounter() const { return counter; }

void Guard::setCounter(int value) { counter = value; }

void Guard::reverseDirection() {
  if (direction.x_dir == lefto.x_dir && direction.y_dir == lefto.y_dir)
    direction = righto;
  else if (direction.x_dir == righto.x_dir && direction.y_dir == righto.y_dir)
    direction = lefto;
  else if (direction.x_dir == up.x_dir && direction.y_dir == up.y_dir)
    direction = down;
  else if (direction.x_dir == down.x_dir && direction.y_dir == down.y_dir)
    direction = up;
}
void Guard::step() {
  position->x_pos += direction.x_dir;
  position->y_pos += direction.y_dir;
}

void Guard::canStep(const QVector<QVector<Element *>> &gameTable,
                    int boardSize) {
  boardSize -= 1;
  int x = position->x_pos;
  int y = position->y_pos;

  if (x + direction.x_dir <= boardSize && x + direction.x_dir >= 0 &&
      y + direction.y_dir <= boardSize && y + direction.y_dir >= 0 &&
      !gameTable[x + direction.x_dir][y + direction.y_dir]->getIsObsticle()) {
    if (counter == 7) {
      counter = 0;

      if (lastPosition == nullptr)
        lastPosition = new pos(position->x_pos, position->y_pos);
      else {
        lastPosition->x_pos = position->x_pos;
        lastPosition->y_pos = position->y_pos;
      }
      position->x_pos += direction.x_dir;
      position->y_pos += direction.y_dir;
    }
  } else
    reverseDirection();
  ++counter;
}

Obstacle::Obstacle(int x, int y) {
  position = new pos(x, y);
  isObsticle = true;
  isLaci = false;
  direction = up;
  //im = new QPixmap(":/img/img/tree.png");
}

Laci::Laci(int x, int y) {
  position = new pos(x, y);
  isObsticle = false;
  isLaci = true;
  lastPosition = nullptr;
  alive = true;
  //im = new QPixmap(":/img/img/laci.png");
  score = 0;
}

bool Laci::getAlive() const { return alive; }

void Laci::die() { alive = false; }

void Laci::step(dir dire, int boardSize,
                const QVector<QVector<Element *>> &gameTable) {
  boardSize -= 1;
  int x = position->x_pos;
  int y = position->y_pos;
  if (x + dire.x_dir <= boardSize && x + dire.x_dir >= 0 &&
      y + dire.y_dir <= boardSize && y + dire.y_dir >= 0 &&
      !gameTable[x + dire.x_dir][y + dire.y_dir]->getIsObsticle()) {
    if (lastPosition == nullptr)
      lastPosition = new pos(position->x_pos, position->y_pos);
    else {
      lastPosition->x_pos = position->x_pos;
      lastPosition->y_pos = position->y_pos;
    }
    position->x_pos += dire.x_dir;
    position->y_pos += dire.y_dir;
  }
}

pos *Laci::getLastPosition() const { return lastPosition; }

void Laci::setLastPosition(pos *value) { lastPosition = value; }

int Laci::getScore() const { return score; }

void Laci::setScore(int value) { score = value; }
void Laci::incScore() { score += 1; }

Empty::Empty(int x, int y) {
  position = new pos(x, y);
  isObsticle = false;
  isLaci = false;
  //im = new QPixmap(":/img/img/Empty.png");
}

Basket::Basket(int x, int y) {
  position = new pos(x, y);
  isObsticle = false;
  isLaci = false;
  //im = new QPixmap(":/img/img/basket.png");
}
