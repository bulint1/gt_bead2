QT += testlib
QT += gui
QT += widgets

CONFIG += qt console warn_on depend_includepath testcase
CONFIG -= app_bundle

TEMPLATE = app

SOURCES +=  tst_testbackground.cpp \
    element.cpp \
    gamebackground.cpp

RESOURCES += \
    res.qrc

HEADERS += \
    direction.h \
    element.h \
    gamebackground.h
