//#include <QThread>
#include <QtTest>

// add necessary includes here
#include "direction.h"
#include "element.h"
#include "gamebackground.h"

class testBackground : public QObject {
  Q_OBJECT

  gamebackgound *Manager;

public:
  testBackground();
  ~testBackground();

private slots:
  void initTestCase();
  void cleanupTestCase();
  void testConstructor();
  void testnewgame();
  void testGameStep();
};

testBackground::testBackground() {}

testBackground::~testBackground() {}

void testBackground::initTestCase() { Manager = new gamebackgound; }

void testBackground::cleanupTestCase() { delete Manager; }

void testBackground::testConstructor() {
  initTestCase();
  QCOMPARE(Manager->getGameRunning(), false);
  QCOMPARE(Manager->getGameTable(9, 9)->getIsLaci(), true);
  QCOMPARE(Manager->getGameTable(0, 6)->getIsObsticle(), false);
  QCOMPARE(Manager->getBoardSize(), 10);
  Manager->start();
  QCOMPARE(Manager->getGameRunning(), true);
  Manager->pause();
  QCOMPARE(Manager->getGameRunning(), false);
  QCOMPARE(Manager->getScore(), 0);
  // cleanupTestCase();
}

void testBackground::testnewgame() {
  initTestCase();
  QCOMPARE(Manager->getGameRunning(), false);
  QCOMPARE(Manager->getGameTable(9, 9)->getIsLaci(), true);
  QCOMPARE(Manager->getGameTable(0, 6)->getIsObsticle(), false);
  QCOMPARE(Manager->getBoardSize(), 10);
  Manager->newGame("map2.txt");
  QCOMPARE(Manager->getGameRunning(), false);
  QCOMPARE(Manager->getGameTable(14, 14)->getIsLaci(), true);
  QCOMPARE(Manager->getGameTable(0, 6)->getIsObsticle(), false);
  QCOMPARE(Manager->getBoardSize(), 15);
  Manager->newGame("map3.txt");
  QCOMPARE(Manager->getGameRunning(), false);
  QCOMPARE(Manager->getGameTable(11, 11)->getIsLaci(), true);
  QCOMPARE(Manager->getGameTable(0, 6)->getIsObsticle(), false);
  QCOMPARE(Manager->getBoardSize(), 12);
  // cleanupTestCase();
}

void testBackground::testGameStep() {
  initTestCase();
  Manager->start();
  Element *temp = Manager->getGameTable(4, 0);
  QCOMPARE(temp, Manager->getGameTable(4, 0));
  for (int i = 0; i < 7; ++i) {
    Manager->stepGame();
    QCOMPARE(temp, Manager->getGameTable(4, 0));
  }
  Manager->stepGame();
  QVERIFY(temp != Manager->getGameTable(4, 0));
  QVERIFY(temp == Manager->getGameTable(4, 1));
  // QCOMPARE(temp, Manager->getGameTable(4, 0));
}

QTEST_APPLESS_MAIN(testBackground)

#include "tst_testbackground.moc"
