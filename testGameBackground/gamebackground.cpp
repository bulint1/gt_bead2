#include "gamebackground.h"
#include <QShortcut>
#include <fstream>
#include <iostream>
gamebackgound::gamebackgound() {
  frame = new QTimer();
  // setMaximumSize(0, 0);
  connect(frame, SIGNAL(timeout()), this, SLOT(stepGame()));
  gameRunning = false;
  laci = nullptr;
  readData("map1.txt");
}

void gamebackgound::start() {
  frame->start(67);
  // stepGame();
  gameRunning = true;
}

void gamebackgound::pause() {
  frame->stop();
  gameRunning = false;
}

bool gamebackgound::getGameRunning() const { return gameRunning; }

void gamebackgound::replace(Guard *guard) { // not sure about this
  if (guard->getLastPosition() != nullptr)
    std::swap(
        gameTable[guard->getPosition()->x_pos][guard->getPosition()->y_pos],
        gameTable[guard->getLastPosition()->x_pos]
                 [guard->getLastPosition()->y_pos]);
  guard->setLastPosition(nullptr);
}

void gamebackgound::stepGame() {
  foreach (Guard *g, guards) {
    if (g->checkAround(gameTable, boardSize))
      laci->die();
    g->canStep(gameTable, boardSize);
  }
  updatePosition();
}

int gamebackgound::getBoardSize() const { return boardSize; }

Element *gamebackgound::getGameTable(int x, int y) { return gameTable[x][y]; }
void gamebackgound::newGame(std::string map) { readData(map); }

void gamebackgound::checkKey(Qt::Key key) {
  if (gameRunning)
    switch (key) {
    case Qt::Key_W:
      laci->step(lefto, boardSize, gameTable);
      break;
    case Qt::Key_A:
      laci->step(up, boardSize, gameTable);
      break;
    case Qt::Key_S:
      laci->step(righto, boardSize, gameTable);
      break;
    case Qt::Key_D:
      laci->step(down, boardSize, gameTable);
      break;
    default:
      break;
    }
}

int gamebackgound::getScore() { return laci->getScore(); }

void gamebackgound::checkGame() {
  foreach (Basket *b, baskets) {
    if (laci->getPosition()->x_pos == b->getPosition()->x_pos &&
        laci->getPosition()->y_pos == b->getPosition()->y_pos) {
      laci->incScore();
      gameTable[b->getPosition()->x_pos][b->getPosition()->y_pos] =
          new Empty(b->getPosition()->x_pos, b->getPosition()->y_pos);
      baskets.removeOne(b);
      delete b;
    }
  }
  if (baskets.size() == 0) {
    frame->stop();
    emit updateScreen();
    emit gameWon();
    gameRunning = false;
    laci->setScore(0);

    // win signal
  } else if (!laci->getAlive()) {
    frame->stop();
    emit gameLost();
    gameRunning = false;
    laci->setScore(0);
    // game over
  }
}

void gamebackgound::readData(std::string file) {
  std::ifstream f(file.c_str());
  if (f.fail())
    exit(1);
  f >> boardSize;
  gameTable.clear();
  delete laci;
  gameTable.resize(boardSize);
  guards.clear();
  baskets.clear();

  for (int i = 0; i < boardSize; ++i) {
    gameTable[i].resize(boardSize);
    for (int j = 0; j < boardSize; ++j) {
      char in;
      f >> in;
      switch (in) {
      case 'N':
        gameTable[i][j] = new Empty(i, j);
        break;
      case 'L':
        laci = new Laci(i, j);
        gameTable[i][j] = laci;
        break;
      case 'P':
        baskets.append(new Basket(i, j));
        gameTable[i][j] = baskets.last();
        break;
      case 'O':
        gameTable[i][j] = new Obstacle(i, j);
        break;
      case '^':
        guards.append(new Guard(i, j, lefto));
        gameTable[i][j] = guards.last();
        break;
      case '<':
        guards.append(new Guard(i, j, up));
        gameTable[i][j] = guards.last();
        break;
      case '>':
        guards.append(new Guard(i, j, down));
        gameTable[i][j] = guards.last();
        break;
      case 'v':
        guards.append(new Guard(i, j, righto));
        gameTable[i][j] = guards.last();
        break;
      default:
        break;
      }
    }
  }
}

void gamebackgound::updatePosition() {
  checkGame();
  foreach (Guard *g, guards) { replace(g); }
  replace(laci);
  emit updateScreen();
}
void gamebackgound::replace(Laci *laci) { // not sure about this
  if (laci->getLastPosition() != nullptr &&
      laci->getLastPosition() != laci->getPosition()) {
    std::swap(gameTable[laci->getPosition()->x_pos][laci->getPosition()->y_pos],
              gameTable[laci->getLastPosition()->x_pos]
                       [laci->getLastPosition()->y_pos]);
    laci->setLastPosition(nullptr);
  }
}
