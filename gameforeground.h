#ifndef GAMEFOREGROUND_H
#define GAMEFOREGROUND_H

#include "gamebackground.h"
#include "selectiondialog.h"
#include <QLCDNumber>
#include <QLabel>
#include <QLayout>
#include <QObject>
#include <QPushButton>
#include <QWidget>

class gameForeground : public QWidget {
  Q_OBJECT
public:
  explicit gameForeground(QWidget *parent = nullptr);

public slots:
  void updateMap();
  void gameWon();
  void gameLost();
  void selectMap();

private slots:
  void playPause();
  void newGame();
  void checkKeyW();
  void checkKeyA();
  void checkKeyS();
  void checkKeyD();
  void incTime();

private:
  QVector<QVector<QLabel *>> imageMatrix; // remove imageamtrix
  QVBoxLayout *mainLayout;
  gamebackgound *Manager;
  QHBoxLayout *topLayout;
  QGridLayout *gridLayout;
  QPushButton *newGameButton;
  QPushButton *playpauseButton;
  QLCDNumber *time;
  QLCDNumber *score;
  QLabel *scoreLabel;
  QLabel *timeLabel;
  QTimer *timer;
  SelectionDialog *dialog;
  void generateBoard();
};

#endif // GAMEFOREGROUND_H
