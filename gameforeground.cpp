#include "gameforeground.h"
//#include <QDebug>
#include <QMessageBox>

void gameForeground::generateBoard() { // remove imageamtrix
  foreach (QVector<QLabel *> vect, imageMatrix)
    foreach (QLabel *e, vect) {
      gridLayout->removeWidget(e);
      delete e;
    }
  imageMatrix.clear();
  imageMatrix.resize(Manager->getBoardSize());
  for (int i = 0; i < Manager->getBoardSize(); ++i) {
    imageMatrix[i].resize(Manager->getBoardSize());
    for (int j = 0; j < Manager->getBoardSize(); ++j) {
      imageMatrix[i][j] = new QLabel;
      gridLayout->addWidget(imageMatrix[i][j], i, j);
    }
  }
  setMinimumSize(80 * Manager->getBoardSize(), 70 * Manager->getBoardSize());
  resize(0, 0);
  updateMap();
}

gameForeground::gameForeground(QWidget *parent) : QWidget(parent) {

  setWindowTitle("Maci Laci");
  Manager = new gamebackgound;
  setMinimumSize(80 * Manager->getBoardSize(), 70 * Manager->getBoardSize());
  dialog = new SelectionDialog(this);
  Manager->setFocus();
  mainLayout = new QVBoxLayout(this);
  topLayout = new QHBoxLayout();
  newGameButton = new QPushButton();
  playpauseButton = new QPushButton();
  time = new QLCDNumber(this);
  score = new QLCDNumber(this);
  score->display(0);
  time->display(0);
  newGameButton->setText("New Game");
  playpauseButton->setText("Play");
  timeLabel = new QLabel("Time:");
  scoreLabel = new QLabel("Score:");
  connect(playpauseButton, SIGNAL(clicked()), this, SLOT(playPause()));
  connect(newGameButton, SIGNAL(clicked()), this, SLOT(selectMap()));
  connect(newGameButton, SIGNAL(clicked()), this, SLOT(newGame()));
  connect(Manager, SIGNAL(gameLost()), this, SLOT(gameLost()));
  connect(Manager, SIGNAL(gameWon()), this, SLOT(gameWon()));
  connect(Manager, SIGNAL(updateScreen()), this, SLOT(updateMap()));
  new QShortcut(QKeySequence(Qt::Key_W), this, SLOT(checkKeyW()));
  new QShortcut(QKeySequence(Qt::Key_S), this, SLOT(checkKeyS()));
  new QShortcut(QKeySequence(Qt::Key_A), this, SLOT(checkKeyA()));
  new QShortcut(QKeySequence(Qt::Key_D), this, SLOT(checkKeyD()));
  new QShortcut(QKeySequence(Qt::Key_Space), this, SLOT(playPause()));

  timer = new QTimer;
  timer->setInterval(1000);
  connect(timer, SIGNAL(timeout()), this, SLOT(incTime()));

  // topLayout->addWidget(Manager);
  topLayout->addWidget(timeLabel);
  topLayout->addWidget(time);
  topLayout->addWidget(scoreLabel);
  topLayout->addWidget(score);
  topLayout->addWidget(newGameButton);
  topLayout->addWidget(playpauseButton);
  gridLayout = new QGridLayout;
  mainLayout->addLayout(topLayout);
  mainLayout->addLayout(gridLayout);
  generateBoard();
  setLayout(mainLayout);
}

void gameForeground::updateMap() { // new game redo felület//refactor replace to
                                   // one method
  for (int i = 0; i < (Manager->getBoardSize()); ++i) {
    for (int j = 0; j < (Manager->getBoardSize()); ++j) {
      imageMatrix[i][j]->setPixmap(*(Manager->getGameTable(i, j)->getIm()));
    }
  }
  score->display(Manager->getScore());
}

void gameForeground::gameWon() {
  timer->stop();
  QMessageBox::information(this, "Game Over!", "You Won!");
  playpauseButton->setEnabled(false);
  playpauseButton->setText("Play");
  time->display(0);
}

void gameForeground::gameLost() {
  timer->stop();
  QMessageBox::information(this, "Game Over!", ("You Died!"));
  playpauseButton->setEnabled(false);
  playpauseButton->setText("Play");
  time->display(0);
}

void gameForeground::selectMap() {}

void gameForeground::playPause() {
  if (Manager->getGameRunning()) {
    Manager->pause();
    playpauseButton->setText("Play");
    timer->stop();
  } else {
    Manager->start();
    playpauseButton->setText("Pause");
    timer->start();
  }
}

void gameForeground::newGame() {
  if (dialog->exec() == QDialog::Accepted) {
    Manager->newGame(dialog->getMap());
    Manager->pause();
    timer->stop();
    generateBoard();
    playpauseButton->setEnabled(true);
    playpauseButton->setText("Play");
    time->display(0);
  }
}

void gameForeground::checkKeyW() { Manager->checkKey(Qt::Key_W); }
void gameForeground::checkKeyA() { Manager->checkKey(Qt::Key_A); }
void gameForeground::checkKeyS() { Manager->checkKey(Qt::Key_S); }
void gameForeground::checkKeyD() { Manager->checkKey(Qt::Key_D); }

void gameForeground::incTime() { time->display(time->value() + 1); }
