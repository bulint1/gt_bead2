#ifndef SELECTIONDIALOG_H
#define SELECTIONDIALOG_H

#include <QDialog>
#include <QGridLayout>
#include <QLabel>
#include <QPushButton>

class SelectionDialog : public QDialog {
  Q_OBJECT
public:
  explicit SelectionDialog(QWidget *parent = nullptr);
  std::string getMap() const;

private slots:
  void setter(std::string value);

private:
  std::string name;
  QLabel *text;
  QVBoxLayout *mainLayout;
  QPushButton *mapA;
  QPushButton *mapB;
  QPushButton *mapC;
  QPushButton *mapRandom;
  QPushButton *cancel;
  QGridLayout *gridLayout;
};

#endif // SELECTIONDIALOG_H
