#ifndef DIRECTION_H
#define DIRECTION_H

struct dir {
  int x_dir;
  int y_dir;
  dir() {
    x_dir = 0;
    y_dir = 0;
  }
  dir(int x, int y) {
    x_dir = x;
    y_dir = y;
  }
};
static dir up(0, -1);
static dir down(0, 1);
static dir right(1, 0);
static dir left(-1, 0);

struct pos {
  int x_pos;
  int y_pos;
  pos(int x, int y) {
    x_pos = x;
    y_pos = y;
  }
  void move(dir direction) {
    x_pos += direction.x_dir;
    y_pos += direction.y_dir;
  }
};
#endif // DIRECTION_H
