#include "selectiondialog.h"

SelectionDialog::SelectionDialog(QWidget *parent) : QDialog(parent) {
  setFixedSize(200, 100);
  setWindowTitle("Options");
  setModal(true);
  name = ":/maps/maps/map1.txt";
  srand(time(nullptr));

  text = new QLabel("Select the Map!");
  mapA = new QPushButton("Map A");
  mapB = new QPushButton("Map B");
  mapC = new QPushButton("Map C");
  mapRandom = new QPushButton("Random");
  cancel = new QPushButton("Cancel");
  connect(mapA, &QPushButton::clicked, this, [this] { setter("map1.txt"); });
  connect(mapA, SIGNAL(clicked()), this, SLOT(accept()));
  connect(mapB, &QPushButton::clicked, this, [this] { setter("map2.txt"); });
  connect(mapB, SIGNAL(clicked()), this, SLOT(accept()));
  connect(mapC, &QPushButton::clicked, this, [this] { setter("map3.txt"); });
  connect(mapC, SIGNAL(clicked()), this, SLOT(accept()));
  connect(mapRandom, &QPushButton::clicked, this, [this] { setter("42"); });
  connect(mapRandom, SIGNAL(clicked()), this, SLOT(accept()));

  gridLayout = new QGridLayout;
  mainLayout = new QVBoxLayout;
  gridLayout->addWidget(mapA, 0, 0);
  gridLayout->addWidget(mapB, 0, 1);
  gridLayout->addWidget(mapC, 1, 0);
  gridLayout->addWidget(mapRandom, 1, 1);
  mainLayout->addWidget(text);
  mainLayout->addLayout(gridLayout);

  setLayout(mainLayout);
}
void SelectionDialog::setter(std::string n) {
  if (n == "42") {
    static const std::string options[] = {"map1.txt", "map2.txt", "map3.txt"};
    int random = rand() % 3;
    name = options[random];
  } else
    name = n;
  this->close();
}

std::string SelectionDialog::getMap() const { return name; }
